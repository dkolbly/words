package words

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"strings"
	"sync"
)

type WordDatabase struct {
	Adjectives []string `json:"adjectives"`
	Nouns      []string `json:"nouns"`
	Adverbs    []string `json:"adverbs"`
	Verbs      []string `json:"verbs"`
	All        []string
}

var cached *WordDatabase
var once sync.Once

func Builtin() *WordDatabase {
	once.Do(func() {
		buf, err := base64.StdEncoding.DecodeString(compressed)
		if err != nil {
			panic(err)
		}
		rd, err := gzip.NewReader(bytes.NewReader(buf))
		if err != nil {
			panic(err)
		}

		dec := json.NewDecoder(rd)
		cached = new(WordDatabase)
		err = dec.Decode(cached)
		if err != nil {
			panic(err)
		}
		all := make([]string,
			len(cached.Adjectives)+
				len(cached.Nouns)+
				len(cached.Adverbs)+
				len(cached.Verbs))
		i := 0
		i += copy(all[i:], cached.Adjectives)
		i += copy(all[i:], cached.Nouns)
		i += copy(all[i:], cached.Adverbs)
		i += copy(all[i:], cached.Verbs)
	})
	return cached
}

type Generator interface {
	Generate(seed uint64) string
}

type simpleGenerator struct {
	vocab [][]string
}

func (g *simpleGenerator) Generate(index uint64) string {
	result := make([]string, len(g.vocab))
	j := len(g.vocab)

	for _, lst := range g.vocab {
		n := uint64(len(lst))
		if n == 0 {
			continue
		}
		j--
		result[j] = lst[index%n]
		index /= n
	}
	return strings.Join(result[j:], " ")
}

func (db *WordDatabase) Compile(pattern ...string) Generator {
	simple := &simpleGenerator{}

	for i := range pattern {
		p := pattern[len(pattern)-i-1]
		switch p {
		case "noun":
			simple.vocab = append(simple.vocab, db.Nouns)
		case "verb":
			simple.vocab = append(simple.vocab, db.Verbs)
		case "adverb":
			simple.vocab = append(simple.vocab, db.Adverbs)
		case "adjective":
			simple.vocab = append(simple.vocab, db.Adjectives)
		case "any":
			simple.vocab = append(simple.vocab, db.All)
		default:
			panic("unknown generator pattern: " + p)
		}
	}
	return simple
}
