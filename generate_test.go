package words

import (
	"testing"
	//"fmt"
)

func TestGenerate(t *testing.T) {
	db := Builtin()
	an := db.Compile("adjective", "noun")
	av := db.Compile("verb", "adverb")
	t.Errorf("yo %q", an.Generate(0))
	t.Errorf("yo %q", an.Generate(1))
	t.Errorf("yo %q", an.Generate(2))
	t.Errorf("av %q", av.Generate(1000))
	t.Errorf("av %q", av.Generate(2000))
	t.Errorf("av %q", av.Generate(3000))
}
