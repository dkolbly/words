# Words

This package is a simple generator for word strings _a la_ docker
container names.

## Importing

```
go get bitbucket.org/dkolbly/words
```

## Example Usage

For example,

```go
package main

import (
	"fmt"
	"hash/crc64"
	"os"

	"bitbucket.org/dkolbly/words"
)

var crc64table = crc64.MakeTable(crc64.ECMA)
var generateNVA = words.Builtin().Compile("noun", "verb", "adverb")

func main() {
	for _, word := range os.Args[1:] {
		key := crc64.Checksum([]byte(word), crc64table)

		fmt.Printf("%s -> %s\n",
			word,
			generateNVA.Generate(key))
	}
}
```

Sample output.  Note that the output is deterministic as a function
of (in this case) the arguments, assuming the builtin word list stays
the same.

```
$ go run main.go -- Hello world friends
-- -> lemming instruct extensively
Hello -> red visit majestically
world -> literature paint outwardly
friends -> hell worry fiercely
```
